const laps = [
  {
    "id": "uniqwo1",
    "name": "WO-0001",
    "status": "open",
    "type": "installation",
    "startDate": "2019-04-18 13:00:00",
    "endDate": "2019-04-18 14:00:00",
    "color": "blue",
    "description": "Install new KoelKast SF-123"
  },
  {
    "id": "uniqwo2",
    "name": "WO-0002",
    "status": "in-progress",
    "type": "maintenance",
    "startDate": "2019-04-18 15:00:00",
    "endDate": "2019-04-18 16:00:00",
    "color": "purple",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo3",
    "name": "WO-0003",
    "status": "closed",
    "type": "failure",
    "startDate": "2019-04-18 17:00:00",
    "endDate": "2019-04-18 18:00:00",
    "color": "black",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo4",
    "name": "WO-0004",
    "status": "in-progress",
    "type": "failure",
    "startDate": "2019-04-18 22:00:00",
    "endDate": "2019-04-18 23:00:00",
    "color": "blue",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo5",
    "name": "WO-0005",
    "status": "open",
    "type": "maintenance",
    "startDate": "2019-04-18 19:00:00",
    "endDate": "2019-04-18 20:00:00",
    "color": "black",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo6",
    "name": "WO-0006",
    "status": "open",
    "type": "installation",
    "startDate": "2019-04-18 08:00:00",
    "endDate": "2019-04-18 09:00:00",
    "color": "purple",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo7",
    "name": "WO-0007",
    "status": "in-progress",
    "type": "maintenance",
    "startDate": "2019-04-18 10:00:00",
    "endDate": "2019-04-18 11:00:00",
    "color": "black",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo8",
    "name": "WO-0008",
    "status": "closed",
    "type": "failure",
    "startDate": "2019-04-18 12:00:00",
    "endDate": "2019-04-18 13:00:00",
    "color": "blue",
    "description": "Check freon on split-system"
  },
  {
    "id": "uniqwo9",
    "name": "WO-0009",
    "status": "closed",
    "type": "installation",
    "startDate": "2019-04-18 06:00:00",
    "endDate": "2019-04-18 07:00:00",
    "color": "blue",
    "description": "Check freon on split-system"
  }
];

export default laps;
