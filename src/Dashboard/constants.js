const getKeyValueList = (objects) => (
  Object.keys(objects).map(obj => ({ value: obj, label: objects[obj] }))
);

const prettyTypes = {
  failure: 'Failure',
  maintenance: 'Maintenance',
  installation: 'Installation',
};

const prettyStatuses = {
  open: 'Open',
  'in-progress': 'In-Progress',
  closed: 'Closed',
};

const prettyColors = {
  black: 'Black',
  purple: 'Purple',
  blue: 'Blue',
};

const statuses = getKeyValueList(prettyStatuses);
const types = getKeyValueList(prettyTypes);
const colors = getKeyValueList(prettyColors);

export {
  types,
  colors,
  statuses,
  prettyTypes,
  prettyColors,
  prettyStatuses
};
