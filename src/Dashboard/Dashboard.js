import React from 'react';
import {
  View, Text, FlatList, TouchableHighlight, TextInput,
  Button, TouchableOpacity, ScrollView, Modal,
} from 'react-native';
import moment from 'moment';
import { getFilteredData } from './utils';
import {
  prettyColors, prettyStatuses, prettyTypes, statuses, types, colors
} from './constants';
import styles from './Dashboard.styles';

const getTime = date => moment(date).format('HH:mm');
const getDate = date => moment(date).format('YYYY-MM-DD');

const selectableLists = {
  status: statuses,
  type: types,
  color: colors
};

class Dashboard extends React.Component {
  state = {
    q: '',
    currentSelectableListName: '',
    modalVisible: false,
    color: [],
    status: [],
    type: [],
  };

  toggleModal = (listName) => {
    this.setState({
      modalVisible: !this.state.modalVisible,
      currentSelectableListName: listName || 'color'
    });
  }

  selectItem = (listName, item) => {
    this.setState({
      [listName]: [...this.state[listName], item]
    });
  }

  removeSelectedItem = (listName, item) => {
    const items = this.state[listName].filter(record => record.value !== item.value);
    this.setState({
      [listName]: items
    });
  }

  renderBoxItem = ({ item }) => (
    <TouchableHighlight>
      <View style={styles.box}>
        <View>
          <View style={styles.boxHeaderTop}>
            <Text>{prettyStatuses[item.status]}</Text>
            <Text>{prettyTypes[item.type]}</Text>
          </View>
          <Text style={styles.heading}>{item.name}</Text>
          <Text>{getDate(item.startDate)} - {getDate(item.endDate)}</Text>
          <Text>{getTime(item.startDate)} - {getTime(item.endDate)}</Text>
          <Text style={[{ backgroundColor: item.color }, styles.boxColor]}>
            {prettyColors[item.color]}
          </Text>
        </View>
        <View>
          <View style={styles.description}>
            <Text style={styles.heading}>Description</Text>
            <Text>{item.description}</Text>
          </View>
        </View>
      </View>
    </TouchableHighlight>
  )

  render() {
    const { q, currentSelectableListName } = this.state;
    const filters = {
      q,
      color: this.state.color.map(item => item.value),
      type: this.state.type.map(item => item.value),
      status: this.state.status.map(item => item.value),
    };
    const filteredData = getFilteredData(filters);
    const currentList = selectableLists[currentSelectableListName];

    return (
      <ScrollView style={styles.container}>
        <View style={{ flex: 1, padding: 16 }}>
          <TextInput
            style={styles.textInput}
            placeholder="Search by name"
            onChangeText={(text) => this.setState({ q: text })}
          />
          <TouchableOpacity
            style={styles.filterButton}
            onPress={() => this.toggleModal('status')}
          >
            <Text style={styles.filterButtonText}>Filter by status</Text>
            <Text style={styles.filterCount}>{filters.status.length}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.filterButton}
            onPress={() => this.toggleModal('type')}
          >
            <Text style={styles.filterButtonText}>Filter by type</Text>
            <Text style={styles.filterCount}>{filters.type.length}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.filterButton}
            onPress={() => this.toggleModal('color')}
          >
            <Text style={styles.filterButtonText}>Filter by color</Text>
            <Text style={styles.filterCount}>{filters.color.length}</Text>
          </TouchableOpacity>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => this.toggleModal(currentSelectableListName)}
          >
            <FlatList
              contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
              style={styles.flatList}
              data={currentList}
              keyExtractor={(item) => item.value}
              renderItem={({ item }) => {
                const isSelected = !!filters[currentSelectableListName].filter(record => record === item.value).length;

                return (
                  <TouchableOpacity
                    style={styles.selectItem}
                    onPress={() => {
                      if(isSelected) {
                        this.removeSelectedItem(currentSelectableListName, item);
                      } else {
                        this.selectItem(currentSelectableListName, item);
                      }
                    }}
                  >
                    <Text>{item.label}</Text>
                    <Text style={[styles.selectItemIcon, isSelected ? styles.selectItemIconActive : {}]} />
                  </TouchableOpacity>
                )
              }}
            />
            <View style={{ margin: 24 }}>
              <Button title="Done" onPress={this.toggleModal} />
            </View>
          </Modal>

        </View>
        <View style={{ flexDirection: 'row' }}>
          <FlatList
            style={styles.flatList}
            data={filteredData}
            keyExtractor={(item) => item.id}
            renderItem={this.renderBoxItem}
          />
        </View>
      </ScrollView>
    )
  }
}

export default Dashboard;
