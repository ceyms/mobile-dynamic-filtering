import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 56,
    marginBottom: 32,
  },
  flatList: {
    padding: 16,
  },
  box: {
    backgroundColor: '#fff',
    padding: 16,
    borderWidth: 1,
    borderColor: '#d4e0de',
    marginBottom: 16,
  },
  boxHeaderTop: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 8
  },
  boxColor: {
    marginTop: 8,
    marginBottom: 8,
    color: '#ffffff',
    padding: 8,
    width: 150
  },
  heading: {
    fontSize: 20,
    marginBottom: 8,
    fontWeight: 'bold'
  },
  selectItem: {
    flex: 1,
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd'
  },
  filterButton: {
    marginBottom: 16,
    padding: 16,
    backgroundColor: '#f9f9f9',
    borderWidth: 1,
    borderColor: '#cccccc',
    color: 'red',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  filterButtonText: {
    color: '#22272d'
  },
  filterCount: {
    color: '#22272d',
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: '#cccccc',
    borderRadius: 2,
    height: 20,
    width: 20,
    textAlign: 'center'
  },
  textInput: {
    marginBottom: 16,
    borderWidth: 1,
    padding: 16,
    borderColor: '#cccccc'
  },
  selectItemIcon: {
    borderWidth: 1,
    borderColor: '#cccccc',
    height: 15,
    width: 15
  },
  selectItemIconActive: {
    backgroundColor: 'green'
  }
});

export default styles;
